<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Serializer;

interface SerializerInterface
{
    public function serialize(): array;

    public function unserialize();
}
